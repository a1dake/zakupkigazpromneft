import sys
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import cfscrape
from lxml import html
import time
import numpy as np
import os
import sqlite3
import urllib3
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "gazprom.db")  # путь к БД
sys.path.append(db_path)
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def add_order_to_db(self, number_order, _id):
        try:
            with sqlite3.connect(db_path) as db:
                sql = db.cursor()
                if _id == 1:
                    sql.execute(f"""INSERT INTO orders(id) VALUES ({number_order})""")
                elif _id == 2:
                    sql.execute(f"""INSERT INTO orders(id2) VALUES ({number_order})""")
                elif _id == 3:
                    sql.execute(f"""INSERT INTO orders(id3) VALUES ({number_order})""")
                db.commit()

        except Exception as e:
            print(f'{e} - err')

    def get_last_order(self, section_id):
            with sqlite3.connect(db_path) as db:
                try:
                    sql = db.cursor()
                    if section_id == 1:
                        sql.execute(f"SELECT max(id) FROM orders")
                    elif section_id == 2:
                        sql.execute(f"SELECT max(id2) FROM orders")
                    elif section_id == 3:
                        sql.execute(f"SELECT max(id3) FROM orders")
                    tuple_order: tuple = sql.fetchone()
                    number_order = tuple_order[0]
                    return number_order
                except Exception as e:
                    print(f'{e} - read')
                    sql.execute("""CREATE TABLE orders (id integer unique, id2 integer unique, id3 integer unique);""")
                    return None

    def get_last_id(self, _id):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        last_id = self.get_last_order(_id)
        return last_id

    def get_urls(self):
        self.session.headers = {
            'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.62'
        }

        ads = []
        page1 = 1
        page2 = 1
        page3 = 1
        last_id_1: int | None = self.get_last_id(1)
        last_id_2: int | None = self.get_last_id(2)
        last_id_3: int | None = self.get_last_id(3)

        status = True
        while status:
            url1 = f'https://zakupki.gazprom-neft.ru/tenderix/?LIMIT=100&PAGE={page1}'
            url2 = f'https://zakupki.gazprom-neft.ru/tenderix/prequalification.php?PAGE={page2}'
            url3 = f'https://zakupki.gazprom-neft.ru/msp/fast/?PAGE={page3}'
            response = self.session.get(url1, timeout=30)
            soup = BeautifulSoup(response.text, 'lxml')
            market = soup.select_one('div[class=purchases-list]')

            response_2 = self.session.get(url2, timeout=30)
            soup_2 = BeautifulSoup(response_2.text, 'lxml')
            market_2 = soup_2.select_one('div[class=purchases-list]')
            
            response_3 = self.session.get(url3, timeout=30)
            soup_3 = BeautifulSoup(response_3.text, 'lxml')
            market_3 = soup_3.select_one('div[class=purchases-list]')

            for items in market.find_all('div', class_='purchase ~purchase-popup'):
                new_urls = items.find('div', class_='purchase-number').find('a').get('href')
                urls = 'https://zakupki.gazprom-neft.ru' + new_urls
                tender_id = int(new_urls.split('ID=')[1])

                if last_id_1 is None and isinstance(tender_id, int):
                    self.add_order_to_db(tender_id, 1)
                    ads.append(urls)
                    status = False
                    break

                if tender_id > last_id_1:
                    self.add_order_to_db(tender_id, 1)
                    ads.append(urls)
                else:
                    status = False
                    break
                page1 += 1
                
            for items in market_2.find_all('div', class_='purchase ~purchase-popup'):
                new_urls = items.find('div', class_='purchase-number').find('a').get('href')
                urls = 'https://zakupki.gazprom-neft.ru' + new_urls
                tender_id = int(new_urls.split('ID=')[1])

                if last_id_2 is None and isinstance(tender_id, int):
                    self.add_order_to_db(tender_id, 2)
                    ads.append(urls)
                    status = False
                    break

                if tender_id > last_id_2:
                    self.add_order_to_db(tender_id, 2)
                    ads.append(urls)
                else:
                    status = False
                    break
                page2 += 1
                
            for items in market_3.find_all('div', class_='purchase ~purchase-popup'):
                new_urls = items.find('div', class_='purchase-number').find('a').get('href')
                urls = 'https://zakupki.gazprom-neft.ru/msp/fast/' + new_urls
                tender_id = int(new_urls.split('ID=')[1])

                if last_id_3 is None and isinstance(tender_id, int):
                    self.add_order_to_db(tender_id, 3)
                    ads.append(urls)
                    status = False
                    break

                if tender_id > last_id_3:
                    self.add_order_to_db(tender_id, 3)
                    ads.append(urls)
                else:
                    status = False
                    break
                page3 += 1
                
        return ads        

    def get_content(self):
        ads = self.get_urls()
        contents = []
        for link in ads:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                                'endDate': '',
                                'scoringDate': '',
                                'biddingDate': ''
                                },

                'customer': {
                                'fullName': '',
                            },
                'contactPerson': {
                                'lastName': '',
                                'firstName': '',
                                'middleName': '',
                                'contactPhone': '',
                                'contactEMail': '',
                                }
            }
            
            try:
                response = self.session.get(link, timeout=30)
                tree = html.document_fromstring(response.content)
                item_data['title'] = str(self.get_title(tree))
                item_data['url'] = str(link)
                item_data['purchaseType'] = str(self.get_type(tree))
                item_data['purchaseNumber'] = str(self.get_number(tree))
                item_data['customer']['fullName'] = str(self.get_customer(tree))
                
                last_name, first_name, middle_name = self.get_names(tree, link)
                item_data['contactPerson']['lastName'] = str(last_name)
                item_data['contactPerson']['firstName'] = str(first_name)
                item_data['contactPerson']['middleName'] = str(middle_name)
                
                item_data['contactPerson']['contactPhone'] = str(self.get_phone(tree))
                item_data['contactPerson']['contactEMail'] = str(self.get_email(tree))

                item_data['procedureInfo']['endDate'] = self.get_end_date(tree)
                item_data['procedureInfo']['scoringDate'] = self.get_scor_date(tree)
                item_data['procedureInfo']['biddingDate'] = self.get_bid_date(tree)

                item_data['lots'] = self.get_lots(tree)

                item_data['attachments'] = self.get_attach(tree)

                contents.append(item_data)
                time.sleep((6-2)*np.random.random()+5)
            except:
                print("Страница с ошибкой ", link)  
        return contents
    
    def get_title(self, tree):
        try:
            data = ''.join(tree.xpath('//div[@class="info-title tender-desktop"]/text()')).strip()
        except:
            data = ''
        return data
    
    def get_type(self, tree):
        try:
            data = ' '.join(''.join(tree.xpath('//*[contains(text(),"Вид процедуры:")]/parent::*/./*[2]/text()')).strip().split())
        except:
            data = ''
        return data
    
    def get_number(self, tree):
        try:
            data = ' '.join(''.join(tree.xpath('//div[@class="info-number tender-desktop"]/*[1]/text()')).strip().split())
        except:
            data = ''
        return data
    
    def get_customer(self, tree):
        try:
            data = ''.join(';'.join(tree.xpath('//*[contains(text(),"Организатор:")]/parent::*/./*[2]/text()')).strip()).split(';')[0].strip() or ''.join(';'.join(tree.xpath('//*[contains(text(),"Заказчик:")]/parent::*/./*[2]/text()[1]')).strip()).split(';')[0].strip()
        except:
            data = ''
        return data
    
    def get_names(self, tree, link):
        try:
            if 'msp/fast' in link:
                try:
                    contact = ''.join(';'.join(tree.xpath('//*[contains(text(),"Контактные данные:")]/parent::*/./*[2]/text()[1]')).strip()).split(';')[0]
                    
                    try:
                        last_name = ''.join(contact.split()[0])
                    except:
                        last_name = ''
                    try:
                       first_name = ''.join(contact.split()[1])
                    except:
                        first_name = ''
                    try:   
                        middle_name = ''.join(contact.split()[2])
                    except:
                        middle_name = ''
                except:
                    last_name = ''
                    first_name = ''
                    middle_name = ''
            else:
                try:
                    contact = ' '.join(''.join(tree.xpath('//*[contains(text(),"Контактное лицо:")]/text()')).split('Контактное лицо:')[1].split())
                    try:
                        last_name = ''.join(contact.split()[0])
                    except:
                        last_name = ''
                    try:
                       first_name = ''.join(contact.split()[1])
                    except:
                        first_name = ''
                    try:   
                        middle_name = ''.join(contact.split()[2])
                    except:
                        middle_name = ''
                except:
                    last_name = ''
                    first_name = ''
                    middle_name = ''
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return last_name, first_name, middle_name
        
    def get_phone(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Тел:")]/./*[1]/text()')) or ''.join(str(tree.xpath('//*[contains(text(),"Телефон:")]/parent::*/./*[2]/a/text()[1]')).split()[0]).strip('" , [ ] { }').strip(" ' ") or '' 
        except:
            data = ''
        return data
        
    def get_email(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"E-mail:")]/./*[1]/text()')) or ''.join(str(tree.xpath('//*[contains(text(),"Email:")]/parent::*/./*[2]/a/text()[1]')).split()[0]).strip('" , [ ] { }').strip(" ' ") or ''
        except:
            data = ''
        return data

    def get_end_date(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата окончания приема:")]/parent::*/./*[2]/text()[1]')).strip()[0:10] or ''
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date
        
    def get_scor_date(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Дата вскрытия:")]/parent::*/./*[2]/text()[1]')).strip()[0:10] or ''
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date
        
    def get_bid_date(self, tree):
        try:
            data = ''.join(tree.xpath('//td[@class="center"]/text()[1]')).strip()[0:10] or ''
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date
        
    def get_lots(self, tree):
        try:
            count = tree.xpath('//table/tbody/tr')
            data = []
            for lot in count:
                lots = {
                        'address': '',
                        'lotItems': []

                    }
                names = {'code': ' '.join(''.join(lot.xpath('./*[1]/text()')).split()),
                         'name': ' '.join(''.join(lot.xpath('./*[2]/text()')).split())}

                adress = ' '.join(''.join(tree.xpath('//*[contains(text(),"Место поставки")]/parent::*/./*[2]/text()[1]')).split()).strip()
                new_adress = adress.split()
                lots['address'] = " ".join(sorted(set(new_adress), key=new_adress.index))

                lots['lotItems'].append(names)
                data.append(lots)
        except:
            return []
        return data
        
    def get_attach(self, tree):
        try:
            data = []
            doc_div = tree.xpath('//div[@class="docs-list"]/div[@class="docs-wrapper"]/div[@class="docs-container"]/div[@class="doc-info"]')
                    
            for doc in doc_div:
                docs = {'docDescription': ''.join(doc.xpath('./a/text()')),
                        'url': 'https://zakupki.gazprom-neft.ru/' + ''.join(doc.xpath('./a/@href'))}
                data.append(docs)
        except:
            return []
        return data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
